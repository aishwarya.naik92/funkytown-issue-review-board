import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,  Route} from "react-router-dom";
import MainPage from '../src/components/MainPage'
import CurrentIssue from './components/CurrentIssue/CurrentIssue';

import MeetingsPage from './components/MeetingsPage'
import CreateMeeting from './components/CreateMeeting';
import EditMeeting from './components/EditMeeting';

import MeetingApp from './components/Meetings/App';
import "./App.css";
import "font-awesome/css/font-awesome.min.css"
import IssueAnalysis from './components/IssueAnalysisDisplay/issue-analysis'

ReactDOM.render(
  <Router>
    <Route exact path = "/">
      <MainPage></MainPage>
    </Route>

    <Route exact path = "/analysis">
      <IssueAnalysis></IssueAnalysis>
    </Route>

    <Route exact path="/currentIssues">
      <CurrentIssue></CurrentIssue>
    </Route>


    <Route exact path="/meetingsPage">
      <MeetingsPage></MeetingsPage>
    </Route>

    <Route exact path="/createMeeting">
      <CreateMeeting></CreateMeeting>
    </Route>

    <Route path="/editMeeting/:id">
      <EditMeeting></EditMeeting>
    </Route>

    <Route  exact path="/meetings">
        <MeetingApp />
    </Route>

  </Router>,
  document.getElementById('root')
);

