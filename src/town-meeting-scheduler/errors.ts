export class MissingMeetingError {

    message:string;
    description:string = "This error means a meeting could not be found";

    constructor(message:string){
        this.message = message;
    }
}