import React, { useState, useRef } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import axios from "axios";

const CreateTaskPopup = ({modal, toggle, save}) => {
    const [location, setLocation] = useState('');
    const [topic, settopic] = useState('');
    const [time, setTime] = useState('');

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [lastClicked, setLastClicked] = useState(null);

    const timeToggle = () => setDropdownOpen(prevState => !prevState);

    const handleChange = (e) => {
        
        const {name, value} = e.target

        if(name === "location"){
            setLocation(value)
        }else if(name === 'topic'){
            settopic(value)
        } else if (name === 'time'){
            setTime(value)
        }
        
    }

    const handleSave = async (e) => {
        e.preventDefault()
        let meetingObj = {}
        meetingObj["location"] = location
        meetingObj["topic"] = topic
        meetingObj["time"] = time;
        localStorage.setItem("token", "funky_bandit")
        const response = await axios.post('http://34.83.141.202/meetings', meetingObj, 
        {
            headers: {
                authorization: localStorage.getItem("token")
            }
        }
        );
        save(meetingObj)
    }

    const Panel = ({ header, children }) => (
        <div style={{ height: "300px" }}>
          <h1>{header}</h1>
          <div>{children}</div>
        </div>
      );

    const onChange = (title) => (...args) => {
        setTime(args[1])
    };

    return (
        
        <Modal animation="false"  isOpen={modal} toggle={toggle} fade={false}>
            <ModalHeader toggle={toggle}>Create Task</ModalHeader>
            <ModalBody>
            
                    <div className = "form-group">
                        <label> Location </label>
                        <input type="text" className = "form-control" value = {location} onChange = {handleChange} name = "location"/>
                    </div>
                    <div className = "form-group">
                        <label>topic</label>
                        <textarea rows = "2" className = "form-control" value = {topic} onChange = {handleChange} name = "topic"></textarea>
                    </div>
                    <Panel header="Time">
                    <Dropdown direction="right" isOpen={dropdownOpen} toggle ={timeToggle} value= {time} onChange = {handleChange} name = "time">
                    <DropdownToggle caret>
                    Pick your time
                    </DropdownToggle>
                            <DropdownMenu>
                                    <DropdownItem onClick={() => setTime("12:00pm")}>12:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("12:30pm")}>12:30pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("1:00pm")}>1:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("1:30pm")}>1:30pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("2:00pm")}>2:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("2:30pm")}>2:30pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("3:00pm")}>3:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("3:30pm")}>3:30pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("4:00pm")}>4:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("4:30pm")}>4:30pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("5:00pm")}>5:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("5:30pm")}>5:30pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("6:00pm")}>6:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("6:30pm")}>6:30pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("7:00pm")}>7:00pm</DropdownItem>
                                    <DropdownItem onClick={() => setTime("7:30pm")}>7:30pm</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                        Last clicked: {time || 'No Time Selected'}
                    </Panel>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={handleSave}>Create</Button>{' '}
                <Button color="secondary" onClick={toggle}>Cancel</Button>
            </ModalFooter>
      </Modal>

    );
};

export default CreateTaskPopup;
