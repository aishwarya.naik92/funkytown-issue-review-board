import axios from "axios";
import React, {useEffect, useState} from 'react';
import CreateTask from '../modals/CreateTask'
import Card from './Card';

const MeetingList = () => {
    const [modal, setModal] = useState(false);
    const [meetingList, setMeetingList] = useState([])
    
    useEffect(async () => {
        const response = await axios.get('http://34.83.141.202/meetings');
            
        localStorage.setItem("meetingList", JSON.stringify(response.data))
        let arr = localStorage.getItem("meetingList")
       
        if(arr){
            let obj = JSON.parse(arr)
            setMeetingList(obj)
        }
    }, [])

    const deleteTask = async (index, event) => {
        let tempList = meetingList
        let meetingObj = tempList[index];
        console.log("Debugging" + meetingObj);
        tempList.splice(index, 1)
        localStorage.setItem("meetingList", JSON.stringify(tempList))
        // const meetingId = event.target.getAttribute('meetingid');
        localStorage.setItem("token", "funky_bandit")
        const response = await axios.delete(`http://34.83.141.202/meetings/${meetingObj.meeting_id}`, {
            headers: {
                authorization: localStorage.getItem("token")
            }
        });
        setMeetingList(tempList)
        window.location.reload()
    }

    const updateListArray = async (obj, index, id) => {
        let tempList = meetingList
        tempList[index] = obj
        debugger
        localStorage.setItem("meetingList", JSON.stringify(tempList))
        localStorage.setItem("token", "funky_bandit")
        const response = await axios.put(`http://34.83.141.202/meetings/${id}`, obj, {
            headers: {
                    authorization: localStorage.getItem("token")
                }
            }
        );
        setMeetingList(tempList)
        window.location.reload()
    }

    const toggle = () => {
        setModal(!modal);
    }

    const saveTask = async (taskObj) => {
        let tempList = meetingList
        tempList.push(taskObj)
        localStorage.setItem("meetingList", JSON.stringify(tempList))

        localStorage.setItem("token", "funky_bandit");

        const response = await axios.post('http://34.83.141.202/meetings', taskObj, 
        {
            headers: {
                authorization: localStorage.getItem("token")
            }
        });

        setMeetingList(meetingList)
        setModal(false)
    }


    return (
        <>
            <div className = "header text-center">
                <h3>ALL MEETINGS LIST</h3>

                <button className = "btn btn-primary mt-2" onClick = {() => setModal(true)} >Create Meeting</button>
            </div>
            <div className = "task-container">
            {meetingList && meetingList.map((obj , index) => <Card meetingObj = {obj} index = {index} deleteTask = {deleteTask} updateListArray = {updateListArray} id= {obj.meeting_id}/> )}
            </div>
            <CreateTask toggle = {toggle} modal = {modal} save = {saveTask}/>
        </>
    );
};

export default MeetingList;
