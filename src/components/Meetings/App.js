import logo from './logo.svg';
import './MeetingApp.css';
import MeetingList from './components/MeetingList'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'

function MeetingApp() {
  return (
    <div className="App">
      <MeetingList />
    </div>
  );
}

export default MeetingApp;
