import axios from "axios";


export default function MeetingsTable2(props){
    const meetings = props.meetings;


    async function deleteMeeting(event){
        console.log(event);
        const meetingId = event.target.getAttribute('meetingid');
        const response = await axios.delete(`http://localhost:3002/meetings/${meetingId}`);
    }
    
    
    return(<table><tr><th>Meeting</th><th>Location</th><th>Time</th><th>Topic</th><th>Delete Meeting</th></tr>
        <tbody>
        {meetings.map(m => <tr key={m.meeting_id}>
                    <td>{m.meeting_id}</td>
                    <td><a href={`/editMeeting/${m.meeting_id}`}>{m.location}</a></td>
                    <td>{m.time}</td>
                    <td>{m.topic}</td>
                    <td><button meetingid = {m.meeting_id} onClick={deleteMeeting}>❌</button></td>
                    </tr>)}
        </tbody>
        </table>)
    }