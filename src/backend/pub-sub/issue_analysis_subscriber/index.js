const {PubSub} = require('@google-cloud/pubsub')
const pubsub = new PubSub({projectId:'funky-town-326721'})

const express = require("express");
const cors = require("cors");

const {Datastore} = require('@google-cloud/datastore')
const datastore = new Datastore();
const app = express();
app.use(express.json());
app.use(cors());

let counter = 0


    const subscription = pubsub.subscription('issue_subscription')
    subscription.on('message', (message)=>{
	const key = datastore.key(['issues']);
        datastore.save({key:key,data:JSON.parse(message.data.toString())})
        console.log(JSON.parse(message.data.toString()));
        console.log('Got a new issue. Issue no: ' + ++counter)
        message.ack()
    })





const PORT = process.env.PORT || 3000
app.listen(PORT, ()=>console.log('Subscriber Application Started'))

