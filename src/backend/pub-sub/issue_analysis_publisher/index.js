const bunyan = require('bunyan');

const {PubSub} = require('@google-cloud/pubsub');
const pubsub = new PubSub({projectId:'funky-town-326721'})
// Imports the Google Cloud client library for Bunyan.

// Creates a Bunyan Cloud Logging client

const cors = require("cors");
// Import express module and create an http server.
const express = require('express');


const app = express();
app.use(cors());
// Install the logging middleware. This ensures that a Bunyan-style `log`
// function is available on the `request` object. This should be the very
// first middleware you attach to your app.


app.use(express.json());


app.post('/submitissue', async(req, res) => {
    console.log("/submitissue - posting an issue");
    
   // logger.info('/submitissue: '+req.body)
    const obj = req.body;
    pubsub.topic('issue').publishJSON(obj); 
    res.status(201)
    res.send("Issue sent");
})


const PORT = process.env.PORT || 3000
app.listen(PORT, ()=>console.log('Application Started'))

