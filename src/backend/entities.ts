
export class Issues{
    constructor( 
        public dateOfPosting: string,
        public dateOfIssue: string,
        public issueDescription: string,
        public location: string,
        public typeOfIssue: string,
        public issueReviewed: boolean,
        public issueHighlighted: boolean,
    ){}
}